<?php
	header('Content-Type: text/html; charset=utf-8');
	header('Access-Control-Allow-Origin: *');
	mb_internal_encoding('UTF-8');
	setlocale(LC_ALL, 'ru_RU.UTF-8');
	ini_set('memory_limit', '-1');
	// 188.120.233.65 
	// telegram 
	// telegram12345 
	// пользователь mysql

	$connectionData['HOST'] = '188.120.233.65';
	$connectionData['USER'] = 'telegram';
	$connectionData['PASSWORD'] = 'telegram12345';
	$connectionData['BASE'] = 'TelcoBot';

	$db = new mysqli($connectionData['HOST'], $connectionData['USER'], $connectionData['PASSWORD'], $connectionData['BASE'], "3306");
	$db->set_charset("utf8");

	/* API FUNCTIONS */
	class User {
		private $id;
		private $username;
		private $phoneNumber;
		private $balance;
		private $tariff;

		function __construct() {
			$this->id = "";
			$this->username = "";
			$this->phoneNumber = "";
			$this->balance = "";
			$this->tariff = "";
		}

		public static function createNew($username, $phoneNumber, $balance, $tariff)
		{
			$this->username = $username;
			$this->phoneNumber = $phoneNumber;
			$this->balance = $balance;
			$this->tariff = $tariff;
			$this->addToDatabase();
		}

		public static function createById($id) {
			$instance = new self();
			$instance->loadById($id);
			return $instance;
		}

		protected function loadById($id) {
			global $db;
			$user = select($db, '*', 'Users', 'Id=\'' . $id . '\'');
			$this->id = $user[0]['Id'];
			$this->username = $user[0]['Username'];
			$this->phoneNumber = $user[0]['PhoneNumber'];
			$this->balance = $user[0]['Balance'];
			$this->tariff = $user[0]['Tariff'];
		}

		protected function loadByPhoneNumber($phoneNumber) {
			global $db;
			$user = select($db, '*', 'Users', 'PhoneNumber=\'' . $phoneNumber . '\'');
			$this->id = $user['Id'];
			$this->username = $user['Username'];
			$this->phoneNumber = $user['PhoneNumber'];
			$this->balance = $user['Balance'];
			$this->tariff = $user['Tariff'];
		}

		function addToDatabase() {
			$username = $this->$username;
			$phoneNumber = $this->$phoneNumber;
			$balance = $this->$balance;
			$tariff = $this->$tariff;

			insert($db, 'Users', 'Username, PhoneNumber, Balance, Tariff', '\'' . $username . '\', \'' . $phoneNumber . '\', \'' . $balance . '\', \'' . $tariff . '\'');
			die('Success');
		}
 
 		function userExistsInDb() {
 			$id = $this->id;

 			$user = select($db, '*', 'Users', 'Id=\'' . $id . '\'');

 			return $user != [];
 		}

 		function getUserInJSON() {
 			$user = [
 				'Id' => $this->id,
 				'Username' => $this->username,
 				'PhoneNumber' => $this->phoneNumber,
 				'Balance' => $this->balance,
 				'Tariff' => $this->tariff
 			];

 			return json_encode($user);
 		}
	}

	if (isset($_GET['userExistsInDb']) and $_GET['userExistsInDb'] != null) {
		$id = $_GET['getUserInJSON'];
		$user = User::createById($id);
		$jsonUser = $user->userExistsInDb();

	}

	if (isset($_GET['getUserInJSON']) and $_GET['getUserInJSON'] != null) {
		$id = $_GET['getUserInJSON'];
		$user = User::createById($id);
		$jsonUser = $user->getUserInJSON();
		echo $jsonUser;
	}

	/* SYSTEM FUNCTIONS */

	function insert($db, $table, $fields, $values) {
		$query = 'INSERT INTO ' . $table . ' (' . $fields . ') VALUES (' . $values . ')';
		// echo $query; die();
		$queryResult = $db->query($query);
		return $queryResult;
	}

	function update($db, $table, $set, $where) {
		$query = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE ' . $where;
		// echo $query; die();
		$queryResult = $db->query($query);
		return $queryResult;
	}

	function select($db, $what, $where, $condition) {
		if (isset($condition)) 
			$query = 'SELECT ' . $what . ' FROM ' . $where . ' WHERE ' . $condition;
		else
			$query = 'SELECT ' . $what . ' FROM ' . $where;
		// echo $query; die();
		$queryResult = $db->query($query);
		$result = array();
		while ($row = $queryResult->fetch_assoc()) {
			array_push($result, $row);
		}
		return $result;
	}

	function delete($db, $from, $where) {
		$query = 'DELETE FROM ' . $from . ' WHERE ' . $where;
		$queryResult = $db->query($query);
		return $queryResult;
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Telco Bot</title>
	<script src="/template/js/jquery.min.js" type="text/javascript"></script>
	<script src="/template/js/bootstrap.min.js" type="text/javascript"></script>
	<link href="template/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="template/css/button-style.css" rel="stylesheet" type="text/css">
</head>
<body>
	
</body>
</html>